import { FacebookProvider } from './../../providers/facebook/facebook.service';
import { LoadingProvider } from './../../providers/loading/loading.service';
import { NativeStorage } from '@ionic-native/native-storage';
import { Component} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { User } from '../../models/user';

/**
 * Generated class for the UserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user',
  templateUrl: 'user.html',
})
export class UserPage {

  msg: any;
  msgReady: boolean = false;

  user: User;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public fbProvider: FacebookProvider,
    public nativeStorage: NativeStorage,
    private loading: LoadingProvider
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserPage');
    //Pega os dados do usuário do native storage
    this.nativeStorage.getItem('user')
    .then((data) => {
      this.user = data.user
      this.msgReady = true;
    }, (error) => {
      console.log(error);
    });
  }

  signOut(): void{
    //Utiliza o loading provider para aparecer um box de loading recebendo a msg
    //desejada para exibir como parametro
    let msg: string = "Saindo...";
    let loading = this.loading.Create(msg);

      //Não sei oq faz aqui :/ mas ele desloga, adicionado facebook.service
      this.fbProvider.signOut().then(() =>{

        //Volta para a pagina de login
        this.navCtrl.setRoot(HomePage);

        //finaliza o loading
        loading.dismiss();

      },(error) => {
        console.log(error);

        //Finaliza o loading caso dê erro
        loading.dismiss();
      });
  }
  
  getPosts(): void
  {
    //Permissões fornecidas pelo Facebook: https://developers.facebook.com/docs/facebook-login/permissions
    //Dependendo da permissão para utilizar publicamente é necessario autorização do Facebook.
    
    let permissions = new Array<string>();

    permissions = ["user_posts"];

    //Parametros da GraphAPI: https://developers.facebook.com/tools/explorer
    //let params = "/me?fields=feed.limit(1){message,created_time,comments{message,reactions}},name";
    let params = "me?fields=feed.limit(1){message,created_time,comments{message,reactions},reactions},name,picture";

    //Chamando fbProvider para utilizar o plugin nativo do facebook
    this.fbProvider.getPosts(params, permissions).then((posts)=>{

      //Salva a resposta em um objeto é necessário criar um objeto, farei um model posteriormente.
      this.msg = posts;

      //Chama o método converterData e passa como parametro a data recebida para formatar.
      this.msg.feed.data[0].created_time = this.fbProvider.formatarData(this.msg.feed.data[0].created_time);

      //Usado na view para confirmar o recebimento da mensagem e exibir não terá uso é apenas para teste
      this.msgReady = true;

    }, (error) => {
      console.log(error);
    });
  }

}
