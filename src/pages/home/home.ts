import { FacebookProvider } from './../../providers/facebook/facebook.service';
import { LoadingProvider } from './../../providers/loading/loading.service';
import { NativeStorage } from '@ionic-native/native-storage';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { UserPage } from '../../pages/user/user';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {


  constructor(
    public navCtrl: NavController,
    public fbProvider: FacebookProvider,
    public nativeStorage: NativeStorage,
    private loading: LoadingProvider
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

  signInWithFacebook(): void {
    //Utiliza o loading provider para aparecer um box de loading recebendo a msg
    //desejada para exibir como parametro
    let msg: string = "Entrando...";
    let loading = this.loading.Create(msg);

    //Permissões fornecidas pelo Facebook: https://developers.facebook.com/docs/facebook-login/permissions
    //Dependendo da permissão para utilizar publicamente é necessario autorização do Facebook.
    let permissions : string[];
    
    permissions = ['email', 'public_profile'];

    //Chama o método .Login do login.service
    this.fbProvider.Login(permissions).then(() =>{

      //Após realizar as funções muda de pagina
      this.navCtrl.setRoot(UserPage);

      //Finaliza o loading
      loading.dismiss();

    },(error) => {

      console.log(error);
      //Caso dê erro finaliza o load
      loading.dismiss();
    });
  }
}
