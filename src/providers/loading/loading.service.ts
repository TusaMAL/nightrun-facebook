import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { LoadingController, Loading } from 'ionic-angular';

/*
  Generated class for the LoadingProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LoadingProvider {

  

  constructor(
    public loadingCtrl: LoadingController,
  ) {
    console.log('Hello LoadingProvider Provider');
  }

  Create(mensagemCarregamento: string): Loading{

    //Cria a caixa de loading: http://ionicframework.com/docs/api/components/loading/LoadingController/
      let loading: Loading = this.loadingCtrl.create({
      content: mensagemCarregamento,
      spinner: 'bubbles',
    });

    //Mostra o loading
    loading.present();

    return loading;
  }
}
