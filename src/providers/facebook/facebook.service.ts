import { User } from './../../models/user';
import { AngularFireAuth } from 'angularfire2/auth';
import { NativeStorage } from '@ionic-native/native-storage';
import { LoadingProvider } from './../loading/loading.service';
import { Facebook } from '@ionic-native/facebook';
import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';
import 'rxjs/add/operator/map';

import * as firebase from 'firebase/app';
import { AngularFireDatabase } from 'angularfire2/database';

/*
  Generated class for the FacebookProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FacebookProvider {

  msg: any;

  constructor(
    public fb: Facebook,
    public loading: LoadingProvider,
    public nativeStorage: NativeStorage,
    public afAuth: AngularFireAuth,
    private db: AngularFireDatabase,
  ) {
    console.log('Hello FacebookProvider Provider');
  }

  Create(user: User): Promise<any>{
    //Adicionando os dados do usuário no firebase
    let usuarios = this.db.list('usuarios');
    return usuarios.set(user.uid, user);
  }

  formatarData(data: string): string
  {
    //usado para formatar data do angular: https://angular.io/api/common/DatePipe
    //http://ionicframework.com/docs/api/components/datetime/DateTime/
    let datePipe = new DatePipe("pt-BR");
    data = datePipe.transform(data, 'dd/MM/yyyy, HH:mm');
    return data;
  }

  getPosts(params: string, permissions: string[]): Promise<any>
  {
    //Método do pluguin nativo do facebook do ionic é usado para pegar o JSON da GraphAPI
    return this.fb.api(params, permissions);
  }

  Login(permissions: string[]): Promise<any>{
    let user: User = new User();
    return this.fb.login(permissions).then(res => {
      //fazendo login com facebook no firebase
      const facebookCredential = firebase.auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
      return firebase.auth().signInWithCredential(facebookCredential).then((dados) => {
        //Salvando os dados da resposta da propriedade providerData é onde estão id, email e etc do usuario
        user = dados.providerData[0];
        //Salvando as informações do usuario no armazenamento do smartphone
        this.nativeStorage.setItem('user', {
          user: user
          });
        //Chamando método create e cadastrando o usuário no Firebase
        this.Create(user);
      });
    });
  }

  signOut(): Promise<any>{

    //Não sei oq faz aqui :/ mas ele desloga 
      return this.afAuth.auth.signOut().then(() => {
        //Remove o "objeto" user do armazenamento do smartphone fazendo com que o usuário tenha que logar novamente
        this.nativeStorage.remove('user');
      });
        
    
  }
  
}
