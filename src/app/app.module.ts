import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Facebook } from '@ionic-native/facebook';
import { NativeStorage } from '@ionic-native/native-storage';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { UserPage } from '../pages/user/user';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { LoadingProvider } from '../providers/loading/loading.service';
import { FacebookProvider } from '../providers/facebook/facebook.service';

export const firebaseConfig = {
  apiKey: "AIzaSyC92arwDt9Tfdm5hDY1OiPRXnMOPi9lue4",
  authDomain: "unimed-experienc-1507038363160.firebaseapp.com",
  databaseURL: "https://unimed-experienc-1507038363160.firebaseio.com",
  projectId: "unimed-experienc-1507038363160",
  storageBucket: "unimed-experienc-1507038363160.appspot.com",
  messagingSenderId: "398287542219"
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    UserPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    UserPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Facebook,
    NativeStorage,
    AngularFireDatabase,
    LoadingProvider,
    FacebookProvider
  ]
})
export class AppModule {}
