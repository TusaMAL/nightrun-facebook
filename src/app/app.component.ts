import { NativeStorage } from '@ionic-native/native-storage';
import { UserPage } from './../pages/user/user';
import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage:any = HomePage;

  constructor(
    platform: Platform, 
    statusBar: StatusBar, 
    splashScreen: SplashScreen,
    nativeStorage: NativeStorage
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      nativeStorage.getItem('user')
      .then((data) => {
        // user is previously logged and we have his data
        // we will let him access the app
        this.nav.setRoot(UserPage);
        splashScreen.hide();
      },(error) => {
        //we don't have the user data so we will ask him to log in
        this.nav.setRoot(HomePage);
        splashScreen.hide();
      });
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}

